
public class WordTuple implements Comparable<WordTuple>{
	String word;
	int count;
	int firstIndex;
	
	
	public WordTuple(String word, int count, int firstIndex) {
		this.word = word;
		this.count = count;
		this.firstIndex = firstIndex;
	}
	
	public String getWord() {
		return word;
	}


	public void setWord(String word) {
		this.word = word;
	}

	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	public int getFirstIndex() {
		return firstIndex;
	}
	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}
	
	@Override
	public int compareTo(WordTuple obj) {
		
		if(this == obj) return 0;
		
		if(this.count < obj.getCount()) {
			return 1;
		}
		else
		if(this.count > obj.getCount()) {
			return -1;
		}
		else {
			if(this.firstIndex > obj.getFirstIndex()) {
				return 1;
			}
			else
			if(this.firstIndex < obj.getFirstIndex()) {
				return -1;
			}
			else
			{
				return 0;
			}
		}
	}
	
	@Override
	public String toString() {
		return word + " " + count + " " + firstIndex;
	}
	
	
}
