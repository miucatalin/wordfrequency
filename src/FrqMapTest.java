import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.SortedSet;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 */

/**
 * @author Catalin
 *
 */
public class FrqMapTest {
	FrqMap frqMap;
	String words = "java class java class code c++";

	
	@Before
	public void initialize() {
		frqMap = new FrqMap();
	}
    
	@Test
	public void testFrqMap() {
		FrqMap frqMap = new FrqMap();
		assertNotNull(frqMap);
	}


	@Test
	public void testGetSortedWordSet() {
		frqMap.countWords(words);
		SortedSet<WordTuple> sortedSet = frqMap.getSortedWordSet();
		WordTuple first = sortedSet.first();
		System.out.println(first.toString());
		assertTrue(first.getWord().equals("java") && first.getCount() == 2 && first.getFirstIndex() == 0);
	}

}
