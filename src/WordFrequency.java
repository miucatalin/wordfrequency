import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.SortedSet;

public class WordFrequency {
	public static void main(String[] args) throws IOException {
		
		
		if(args.length != 2) {
			System.out.println("Input and output files should be given as CLI parameter");
		}
		else
		{
			String inputFile = args[0];
			String outputFile = args[1];
			outputResult(wordsFrequency(inputFile), outputFile);
		}

				
	}
	
	private static SortedSet<WordTuple> wordsFrequency(String inputFile) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(inputFile));

		String newLine = null;
		FrqMap frqMap = new FrqMap();
		
		while((newLine = br.readLine()) != null) {
			frqMap.countWords(newLine);
		}
		br.close();
		
		return frqMap.getSortedWordSet();
	}
	
	private static void outputResult(SortedSet<WordTuple> wordSet, String outputFile) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(outputFile));
		
		for(WordTuple tuple : wordSet ) {
			if(tuple.getCount() > 1) {
				bufferedWriter.write(tuple.toString());
				bufferedWriter.newLine();
			}
		}
		bufferedWriter.close();
	}

}
