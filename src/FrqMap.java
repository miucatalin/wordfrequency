import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class FrqMap {
	Map<String, WordTuple> frqMap;
	int wordIndex;
	
	public FrqMap() {
		frqMap = new HashMap<>();
		wordIndex = 0;
	}
	
	public void countWords(String line) {
		String[] words = line.split(" ");
		for (String word : words) {
			if (!frqMap.containsKey(word)) {
				frqMap.put(word, new WordTuple(word, 1, wordIndex));
			} 
			else {
				WordTuple tuple = frqMap.get(word);
				tuple.setCount(tuple.getCount() + 1);
			}
			wordIndex++;
		}
	}
	
	public SortedSet<WordTuple> getSortedWordSet() {
		
		SortedSet<WordTuple> wordSet = new TreeSet<>();
		Set<Map.Entry<String, WordTuple>> entrySet = frqMap.entrySet();
		
		for(Map.Entry<String, WordTuple> entry : entrySet) {
			wordSet.add(entry.getValue());
		}
		
		return wordSet;
	}

}
